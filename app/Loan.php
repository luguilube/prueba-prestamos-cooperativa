<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $table = 'loans';

    protected $fillable = [
        'id', 'amount', 'authorization_date', 'tentative_delivery_date', 'final_payment_date', 'fk_id_user'
    ];

}
